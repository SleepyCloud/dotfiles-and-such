from bs4 import BeautifulSoup
import requests

#get the page
soup = BeautifulSoup(requests.get('https://news.ycombinator.com').text)

#select links within cells with title class
for row in soup.select('td.title > a'):
    #print the contents out
    if not row.text == 'More':
        print row.text
        print row['href'] + "\n"
