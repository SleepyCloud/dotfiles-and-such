call pathogen#infect()
let g:pymode_lint_checker = "pyflakes"
let g:syntastic_cpp_include_dirs = ['usr/local/Qt-5.0.2/bin']
let g:SuperTabDefaultCompletionType = "<C-X><C-O>"
let g:SuperTabDefaultCompletionType = "context"
set t_Co=256

colorscheme jellybeans

"improve status bar
set statusline=%F%m%r%h%w\ FORMAT=%{&ff}\ TYPE=%Y\ ASCII=\%03.3b\ HEX=\%02.2B\ POS=04l,%04v\ %p%%\ LEN=%L
set laststatus=2
"add line numbers
set nu
set relativenumber

"ignore stuff that won't open
let g:fuzzy_ignore = "*.class;*.png;*.PNG;*.JPG;*.jpg;*.GIF;*.gif;vendor/**;coverage/**;tmp/**;rdoc/**;*~"

set nocompatible


set autoindent
set nosmartindent

set wildignore=*.o,*~,*.pyc,*.class

set expandtab
set smarttab

"turn on syntax highlighting
syntax enable
set syntax=automatic

set showcmd

"show matching brackets
set showmatch

filetype on 
filetype plugin on
filetype indent on

set shiftwidth=4
set tabstop=4
set softtabstop=3

set wildmenu
set wildmode=list:longest,full

"Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

set ignorecase
set smartcase

"go back to command mode
inoremap jj <Esc>
inoremap JJ <Esc>

"set auto/smart indent
set ai
set si

set hlsearch

"opens new buffer in current folder
map <F9> :tabedit<c-r>=expand("%:p:h")<cr>/<cr>



set autoread


"no arrow keys in insert or command mode
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

imap <left> <nop>
imap <right> <nop>
imap <up> <nop>
imap <down> <nop>

"set the leader key
let mapleader = ","

nmap <leader>h :nohl<cr>
nnoremap <leader>r :!javac *.java && java main<cr>

"make Y behave as it should
nnoremap Y y$

"leader mappings
"saves/closes
map <leader>q :q!<cr>
map <leader>Q :q!<cr>
map <leader>w :w!<cr>
map <leader>W :w!<cr>
"switches pane in a split
map <leader>n <C-W>k
map <leader>m <C-W>l
map <leader>b <C-W>j
map <leader>v <C-W>h

"enters the shell
map <leader>t :sh<cr>
"moves to next/previous tabs
map <leader>p :tabn<cr>
map <leader>o :tabp<cr>

"open and close folds
nmap <leader>c zc
nmap <leader>x zo

"show all the errors
nnoremap <leader># :Error<cr>

nnoremap <leader>vi :vsplit ~/.vimrc<cr>
nnoremap <leader>1 :vsplit .<cr>
nnoremap <leadeR>git :!git init && git add . && git commit -m "Commit" && git push -u origin master<cr>

"run ace4
nnoremap <leader>z :w<cr>:!gcc main.c -o shell && ./shell<cr>

autocmd VimEnter * call LoadSession()
autocmd VimLeave * call SaveSession()

function! SaveSession()
    execute  'mksession! ~/.vim/sessions/session.vim'
endfunction

function! LoadSession()
    if argc() == 0
        execute 'source ~/.vim/sessions/session.vim'
    endif
endfunction

autocmd FileType java set omnifunc=javacomplete#Complete
autocmd FileType java setlocal completefunc=javacomplete#CompleteParamsInfo
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType php set omnifunc=phpcomplete#CompletePHP

set dictionary="/usr/dict/words"
